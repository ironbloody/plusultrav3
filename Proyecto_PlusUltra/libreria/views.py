from email import message
from django.shortcuts import render, redirect
from django.http import HttpResponse
from pyparsing import autoname_elements
from django.contrib import messages

from .models import *
from .forms import ArticuloForm, RegisterForm
from django.contrib.auth import authenticate, login, logout
from django import template
from django.contrib.auth.models import Group 

from django.http import FileResponse
import csv
import io
from reportlab.pdfgen import canvas
from django.http import JsonResponse
import json
import datetime
# Create your views here.
def Registro(request):
        if request.user.is_authenticated:
                return redirect('inicio')
        else:
                form = RegisterForm()
                if request.method == 'POST':
                        form = RegisterForm(request.POST)
                        if form.is_valid():
                                form.save()
                                user = form.cleaned_data.get('username')
                                messages.success(request, 'Se crea la cuenta exitosamente para el usuario ' + user)
                                return redirect('login')
			
                context = {'form':form}
        return render(request, 'paginas/registro.html', context)

def Login(request):
        if request.user.is_authenticated:
                return redirect('inicio')
        else:
                if request.method == 'POST':
                        username = request.POST.get('username')
                        password =request.POST.get('password')

                        user = authenticate(request, username=username, password=password)

                        if user is not None:
                                        login(request, user)
                                        return redirect('inicio')
                        else:
                                messages.info(request, 'Username OR password is incorrect')

        context = {}
        return render(request, 'paginas/login.html')
def Logout(request):
        logout(request)
        messages.success(request, 'Yor Were logged out')
        return redirect('inicio')

def inicio(request):
        if request.user.is_authenticated:
                usuario = request.user
                orden, created = Orden.objects.get_or_create(usuario=usuario, completado=False)
                items = OrdenItem.objects.all()
                cartItems = orden.get_cart_items

        else:
                items = []
                orden ={'get_cart_total':0, 'get_cart_items':0}
                cartItems = orden['get_cart_items']

        context = {'cartItems': cartItems}
        return render(request, 'paginas/inicio.html', context)

def home(request): 
        return render(request, 'paginas/home.html')

def libros(request): 
        if request.user.is_authenticated:
                usuario = request.user
                orden, created = Orden.objects.get_or_create(usuario=usuario, completado=False)
                items = OrdenItem.objects.all()
                cartItems = orden.get_cart_items

        else:
                items = []
                orden ={'get_cart_total':0, 'get_cart_items':0}
                cartItems = orden['get_cart_items']

        libros = Articulo.objects.filter(tipo = 1).exclude(stock=0)
        context = {'cartItems': cartItems, 'libros': libros}
        return render(request, 'libros/libros.html', context)
def mangas(request): 
        if request.user.is_authenticated:
                usuario = request.user
                orden, created = Orden.objects.get_or_create(usuario=usuario, completado=False)
                items = OrdenItem.objects.all()
                cartItems = orden.get_cart_items

        else:
                items = []
                orden ={'get_cart_total':0, 'get_cart_items':0}
                cartItems = orden['get_cart_items']
        mangas = Articulo.objects.filter(tipo = 3).exclude(stock=0)
        context = {'cartItems': cartItems, 'mangas': mangas}
        return render(request, 'libros/mangas.html', context)

def comics(request): 
        if request.user.is_authenticated:
                usuario = request.user
                orden, created = Orden.objects.get_or_create(usuario=usuario, completado=False)
                items = OrdenItem.objects.all()
                cartItems = orden.get_cart_items

        else:
                items = []
                orden ={'get_cart_total':0, 'get_cart_items':0}
                cartItems = orden['get_cart_items']
        comics = Articulo.objects.filter(tipo = 4).exclude(stock=0)
        context = {'cartItems': cartItems, 'comics': comics}
        return render(request, 'libros/comics.html', context)

def pedidos(request): 
        if request.user.is_authenticated:
                usuario = request.user
                orden, created = Orden.objects.get_or_create(usuario=usuario, completado=False)
                items = OrdenItem.objects.all()
                cartItems = orden.get_cart_items

        else:
                items = []
                orden ={'get_cart_total':0, 'get_cart_items':0}
                cartItems = orden['get_cart_items']

        if usuario.email == "admin@admin.com":
                pedidos = DireccionEntrega.objects.all()
        else:
                pedidos = DireccionEntrega.objects.filter(usuario=usuario)

        context = {'cartItems': cartItems, 'pedidos': pedidos}
        return render(request, 'libros/pedidos.html', context)

def crear_libro(request):
        formulario = ArticuloForm(request.POST or None, request.FILES or None)
        if formulario.is_valid():
                formulario.save()
                return redirect('libros')

        return render(request, 'libros/crear_libro.html', {'formulario': formulario})

def crear_manga(request):
        formulario = ArticuloForm(request.POST or None, request.FILES or None)
        if formulario.is_valid():
                formulario.save()
                return redirect('mangas')

        return render(request, 'libros/crear_manga.html', {'formulario': formulario})

def crear_comic(request):
        formulario = ArticuloForm(request.POST or None, request.FILES or None)
        if formulario.is_valid():
                formulario.save()
                return redirect('comics')

        return render(request, 'libros/crear_comic.html', {'formulario': formulario})
        
def editar(request, id):
        libro = Articulo.objects.get(id=id)
        formulario = ArticuloForm(request.POST or None, request.FILES or None, instance=libro)
        if formulario.is_valid() and request.POST:
                formulario.save()
                return redirect('libros')
        return render(request, 'libros/editar.html', {'formulario': formulario})

def eliminar(request, id):
        libro = Articulo.objects.get(id=id)
        libro.delete()
        return render('libros')

def cart(request):
  
        if request.user.is_authenticated:
                usuario = request.user
                orden, created = Orden.objects.get_or_create(usuario=usuario, completado=False)
                items = OrdenItem.objects.all()
                cartItems = orden.get_cart_items

   
        else:
                items = []
                orden ={'get_cart_total':0, 'get_cart_items':0}
                cartItems = orden['get_cart_items']
        context={'items':items, 'orden': orden, 'cartItems': cartItems}
        return render(request, 'paginas/cart.html', context)

def clear(request):
        items = OrdenItem.objects.all()
        for x in items:
                x.delete()
        return redirect('inicio')
def checkout(request):
        if request.user.is_authenticated:
                usuario = request.user
                orden, created = Orden.objects.get_or_create(usuario=usuario, completado=False)
                items = OrdenItem.objects.all()

        else:
                items = []
                orden ={'get_cart_total':0, 'get_cart_items':0}
        
        context={'items':items, 'orden': orden}
        return render(request, 'paginas/checkout.html', context)

def updateItem(request):
	data = json.loads(request.body)
	productId = data['productId']
	action = data['action']
	print('Action:', action)
	print('Product:', productId)

	usuario = request.user
	product = Articulo.objects.get(id=productId)
	order, created = Orden.objects.get_or_create(usuario=usuario, completado=False)

	ordenItem, created = OrdenItem.objects.get_or_create(orden=order, producto=product)

	if action == 'add':
		ordenItem.cantidad = (ordenItem.cantidad + 1)
	elif action == 'remove':
		ordenItem.cantidad = (ordenItem.cantidad - 1)

	ordenItem.save()

	if ordenItem.cantidad <= 0:
		ordenItem.delete()

	return JsonResponse('Item was added', safe=False)

def processOrder(request): 
        usuario = request.user
        query_cart = OrdenItem.objects.all()
        transaction_id = datetime.datetime.now().timestamp()
        items = OrdenItem.objects.all()
        articulos = []
        for item in items:
                articulos.append(item.producto)
        print(articulos)
        data = json.loads(request.body)
        
        if request.user.is_authenticated:
                usuario = request.user
                orden, created = Orden.objects.get_or_create(usuario=usuario, completado=False)
                total = (data['form']['total'])
                orden.transaccion_id = transaction_id
                if str(total) == str(orden.get_cart_total):
                        orden.completado = True
                orden.save()
                #items = OrdenItem.objects.all()
                #orden.update(producto = items)
                if orden.shipping == True:
                        DireccionEntrega.objects.create(
                                usuario = usuario,
                                orden = orden,
                                direccion = data['shipping']['address'],
                                ciudad = data['shipping']['city'],
                                region = data['shipping']['state'],
                                codigo_postal = data['shipping']['zipcode'],
                                producto = articulos,
                        )

                # Se descuenta el stock de con la cantidad del producto comprado al realizar la paga, este stock será un int normal por ahora, pero funciona
                for see_cart in query_cart:
                        Filtrando = Articulo.objects.get(nombre = see_cart.producto)
                        Filtrando.stock = Filtrando.stock - see_cart.cantidad
                        if Filtrando.stock < 0:
                                Filtrando.stock = 0
                        Filtrando.save()
        else:
                print('user is not login')
        return JsonResponse('Payment complete', safe=False)

register = template.Library()

def enviado(request, id):

        user = request.user
        enviado = DireccionEntrega.objects.filter(id=id)
        enviado.update(Fecha_enviado=datetime.date.today())
        return redirect("pedidos")



# Para exportar un CSV, el cual obtiene de los articulos el nombre, precio y el stock
def article_list(request):
    response = HttpResponse(content_type='text/csv')  
    response['Content-Disposition'] = 'attachment; filename="Articulos.csv"'  
    queryset = Articulo.objects.all()
    writer = csv.writer(response)
    writer.writerow(['Nombre', 'Precio', 'Stock'])
    for article in queryset:  
        writer.writerow([article.nombre, article.precio, article.stock])  
    return response 

# Acá es donde se crea el PDF de ver el horario
def horario(request):
    buffer = io.BytesIO()
    x = canvas.Canvas(buffer)
    x.drawString(200, 800, "Horarios de disponibilidad Local PlusUltra")
    x.drawString(10, 750, "Lunes a viernes: 11:00 - 14:00 y 15:00 - 19:00")
    x.drawString(10, 700, "Sabado: 11:00 - 14:00 y 15:00 - 18:00")
    x.drawString(10, 650, "Plataforma de atención:")
    x.drawString(10, 600, "Instagram: @plusultralibreria")

    x.showPage()
    x.save()
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename='Horario_e_informacion.pdf')

def grafico(request):
    labels = []
    data = []
    queryset = Articulo.objects.order_by('tipo')[:5]
    for article in queryset:
        labels.append(article.nombre)
        data.append(article.stock)
    return render(request, 'paginas/grafico.html', {'labels': labels, 'data': data,})
@register.filter(name='has_group') 
def has_group(user, group_name):
    group =  Group.objects.get(name=group_name) 
    return group in user.groups.all() 