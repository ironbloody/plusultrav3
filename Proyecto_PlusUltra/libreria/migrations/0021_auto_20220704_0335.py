# Generated by Django 3.2.8 on 2022-07-04 03:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('libreria', '0020_remove_orden_producto'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='direccionentrega',
            name='producto',
        ),
        migrations.AddField(
            model_name='orden',
            name='producto',
            field=models.ManyToManyField(to='libreria.Articulo'),
        ),
    ]
