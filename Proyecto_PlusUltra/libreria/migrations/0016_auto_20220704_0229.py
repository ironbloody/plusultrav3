# Generated by Django 3.2.8 on 2022-07-04 02:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('libreria', '0015_auto_20220704_0220'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ordenitem',
            name='producto',
        ),
        migrations.AddField(
            model_name='ordenitem',
            name='producto',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='libreria.articulo'),
        ),
    ]
